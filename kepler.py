#! /usr/bin/env python3

import pygame
import sys
import json
from planete import Planete
import random

FPS = 120
WINDOWWIDTH = 1800
WINDOWHEIGHT = 1600
ARRIERE_PLAN = (25,17,30)


class Quitte(Exception ):
    pass

def isQuitEvent(event):
    return (event.type == pygame.QUIT or
            (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE))

def handleKey(event):
    print("appui sur la touche", event.key)

def handleClick(event):
    print("Clic à la position", event.pos)

def handleEvents():
    for event in pygame.event.get():
        # pour chaque évènement depuis le dernier appel de cette fonction
        if isQuitEvent(event):
            raise Quitte
        elif event.type == pygame.KEYDOWN:
            handleKey(event)
        elif event.type == pygame.MOUSEBUTTONDOWN:
            print("clic")

def drawApp(s_temp, s_pers, ecran, univers):
    """
    Redessine l'écran.
    """
    s_temp.fill((0,0,0,0)) # on remplit avec du transparent
    for objet in univers:               
        objet.dessine(s_pers, s_temp)  #on affiche les éléments de la liste 'univers'
    ecran.fill(ARRIERE_PLAN)
    ecran.blit(s_pers, (0,0))
    ecran.blit(s_temp, (0,0))
    pygame.display.update()


def main():

    # initialisation des paramètres de l'application
    temps = 0
    pygame.init()
    FPSCLOCK = pygame.time.Clock()
    pygame.display.set_caption('Solar System')
    info = pygame.display.Info()
    WINDOWWIDTH, WINDOWHEIGHT = info.current_w,info.current_h
    ecran = pygame.display.set_mode((0,0), pygame.FULLSCREEN)
    s_pers = pygame.Surface((WINDOWWIDTH, WINDOWHEIGHT), pygame.SRCALPHA)
    s_temp = pygame.Surface((WINDOWWIDTH, WINDOWHEIGHT), pygame.SRCALPHA)
    pygame.mixer.init()
    pygame.mixer.music.load('HZ.OGG')
    pygame.mixer.music.play()


    # création des planètes et satellites

    soleil = Planete(100, 0, (WINDOWWIDTH/2,WINDOWHEIGHT/2), 0, (255,100,0)) #étoile centrale

    mercure = soleil.add_satellite(7,125,5,(204,204,255), False, -2)

    venus = soleil.add_satellite(14,150,3.5, (255,204,102)) 

    terre = soleil.add_satellite(15,180,3, (65,105,225), False, -1) 
    lune = terre.add_satellite(2, 25, 5, (204,204,240))

    mars = soleil.add_satellite(8,220,2.25, (255,0,0)) 

    ceinture = soleil.add_satellite(10, 350, 5, (176,196,222), False, 1.5)
    for i in range(3):
        satellite = ceinture.add_satellite(random.randint(5,10),random.randint(25,50),(random.randint(-10,10)), (176,196,222))
        ceinture = satellite

    jupiter = soleil.add_satellite(75,500,1, (255,165,0), False, -1)

    saturne = soleil.add_satellite(65,750,0.5, (255,200,0), False, 2.5) 
    for i in range(7):
        satellite = saturne.add_satellite(i+1,100+7*i,(random.randint(-10,10)), (160,82,45))


    uranus = soleil.add_satellite(30,1000,0.2, (200,208,255), False, 1/3)
    for i in range(3):
        satellite = uranus.add_satellite(i+1,50+5*i,(random.randint(-10,10)), (255,255,255))


    neptune = soleil.add_satellite(30,1200,0.1, (200,208,255))

    univers = [soleil]

    ecran.fill(ARRIERE_PLAN)

    while True:  #boucle principale
        try:
            handleEvents()
            drawApp(s_temp, s_pers, ecran, univers)

            temps_ecoule = FPSCLOCK.tick(FPS)
            for objet in univers:                       # on appelle la methode avance() de Planete pour les animer 
                objet.avance(temps_ecoule)

        except Quitte:
            break

    pygame.quit()
    sys.exit(0)

main()
